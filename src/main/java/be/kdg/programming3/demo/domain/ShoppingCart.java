package be.kdg.programming3.demo.domain;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Product> productList = new ArrayList<>();

    public void addProduct(Product p){
        productList.add(p);
    }

    public List<Product> getProductList() {
        return productList;
    }
}
