package be.kdg.programming3.demo.presentation;

import be.kdg.programming3.demo.domain.Product;
import be.kdg.programming3.demo.domain.ShoppingCart;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/webshop")
public class WebshopController {
    private static final Logger logger = LoggerFactory.getLogger(WebshopController.class);
    @GetMapping
    public String getHomeView(HttpSession session, Model model){
        logger.info("Getting Webshop index page");
        ShoppingCart shoppingCart = loadShoppingCart(session);
        //model.addAttribute("shoppingCart", shoppingCart);//you can also use the ${session} object in Thymeleaf!
        return "index";
    }
    @PostMapping
    public String processAddProduct(String name, HttpSession session, Model model){
        logger.info("Trying to add " + name + " to the shoppingcart...");
        ShoppingCart shoppingCart = loadShoppingCart(session);
        shoppingCart.addProduct(new Product(name));
        //model.addAttribute("shoppingCart", shoppingCart);//you can also use the ${session} object in Thymeleaf!
        return "index";
    }

    private ShoppingCart loadShoppingCart(HttpSession session){
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        if (shoppingCart==null) {
            logger.info("Creating new shopping cart!");
            shoppingCart = new ShoppingCart();
            session.setAttribute("shoppingCart", shoppingCart);
        }
        return shoppingCart;
    }
}
